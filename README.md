# ref-card-03

ref-card-03 is a spring boot application that allows developer / systems engineer to dockerize a maven project

## Prerequisites for Windows-user (Win 10 / 11)

To be noted: you wont need any prior knowledge, you just need to follow the instruction on the websites below

In this section we will show you how to install the ref-card-03 project without any prior knowledge.

* Install the latest WSL 2 version  --> https://learn.microsoft.com/de-de/windows/wsl/install

* Install the latest version of Docker on your WSL 2 --> https://docs.docker.com/engine/install/ubuntu/

* Install the latest Visual Studio Code --> https://learn.microsoft.com/en-us/windows/wsl/tutorials/wsl-vscode


## Prerequisites for Debian based user (recommended version: Debian 11 and Ubuntu 22.04)

To be noted: also here you wont need any prior knowledge.

* Make sure your Linux distribution is updated

```
$ sudo apt update && sudo apt upgrade
```

* Install the latest version of Docker --> https://docs.docker.com/engine/install/ubuntu/


## Installing ref-card-03

*Download the dockerfile and pom file*

Linux:
```
$ sudo wget https://gitlab.com/double_dd/ref-card-03/-/raw/main/Dockerfile?inline=false && sudo wget https://gitlab.com/double_dd/ref-card-03/-/raw/main/pom.xml?inline=false
```

Windows:

*Paste the following 2 links in your browser*

https://gitlab.com/double_dd/ref-card-03/-/raw/main/Dockerfile?inline=false
https://gitlab.com/double_dd/ref-card-03/-/raw/main/pom.xml?inline=false


## Using ref-card-03 for Linux and Windows (in WSL 2)

To use ref-card-03, follow these steps:

```
$ sudo docker build -t ref-card-03.jar .
```

```
$ sudo docker run -p 8080:8080 ref-card-03.jar
```

Via localhost:8080 you have access to your maven project
